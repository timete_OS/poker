

unsigned int power(unsigned int k, int n)
{
    /*
    only ever gets used for cubes or squares
    no need to get fancy with fast exponentiation
    */
    unsigned int tmp = k;
    while(--n > 0)
        k = k*tmp;
    return k;
}

unsigned long int choose(unsigned n, unsigned k) 
{
    /*
    return binomial coef of B(n, k)
    in french it's just "k parmi n", which much
    better describe what this coefficient truly is
    */
    unsigned long int r = 1;
    unsigned long int d;
    if (k > n) return 0;
    for (d=1; d <= k; d++)
    {
        r *= n--;
        r /= d;
    }
    return r;
}