int power(int k, int n);

unsigned long int choose(unsigned n, unsigned k);

int value(char cards);

char* count_element(char* figures);

int is_straight(int* hash_figures);

_Bool is_color(char* cards);

_Bool compare_array(int* a, int* b, int lenght);

unsigned int score_high_card(int* hash_figures);

unsigned int score_one_pair(int* hash_figures);

unsigned int score_two_pair(int* hash_figures);

unsigned int score_three(int* hash_figures);

unsigned int score_straight_flush(int s);

unsigned int score_straight(char* cards, int s);

unsigned int score_flush(int* hash_figures);

unsigned int score_full(int* hash_figures);

unsigned int score_four(int* hash_figures);

unsigned int score(char* cards);

_Bool test_score(void);