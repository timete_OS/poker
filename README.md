# poker

this isn't even a MVP. It's more like an embryo of an MVP.

In some variant of poker (the one from Jojo for exemple), one is given the choice to 
change from none to three cards of his hand.
What this program does is giving the better choice possible when changin those cards.
To do so, every hand is associated with a score, representing it's strenght.
Then the program maximize the expectency of the score.

A hand is a ten character string, representing five cards;

a card is :
first character is the number or figure : 2, 3, 4, 5, 6, 7, 8, 9, T, J, D, K, A
second character is the color : S, H, D, C

A valid input is therefore : JSTHTD8S7H

Theorically you can just compile the program by using the command :

make

You need to be on the /poker folder, also.

Then a few exemple of how the program works :

>>>./poker 2SAH5H6S7S
>>>AH 5H

>>>./poker
>>>

>>>./poker 2093I20
>>>Segmentation fault (core dumped)

lol there aren't any checks of any kind, like ever.

>>>./poker ASKSQSJSTS
>>>
there's nothing to change, you already got the strongest hand of the game