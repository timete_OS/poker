
#include <make.h>
#include <stdlib.h>


char rank[13] = "AKQJT98765432";



int value(char card)
{
    int k = 0;
    while (rank[k] != card)
        k++;
    return k;
}

char* count_element(char* figures)
{
    //never used
    int hash_figures[13] = {0};
    for (int i = 0; i < 5; ++ i)
        hash_figures[value(figures[i])]++;
    
    char* c = malloc(sizeof(char)*5);
    for (int i = 0; i < 13; ++i)
    {
        c[hash_figures[i]] = rank[i];
    }
    return c;
}

_Bool compare_array(int* a, int* b, int lenght)
{
    int h = 1;
    while (--lenght >= 0)
        h = (a[lenght] == b[lenght]) && h;
    return h;
}

int is_straight(int* hash_figures)
{
    _Bool b = 1;
    int c[13] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1};
    if (compare_array(hash_figures, c, 13))
        return 12;
    int i = 0;
    while(hash_figures[i] == 0)
        i++;
    for (int j=i; j < i+5; ++j)
        b = (hash_figures[j]==1)&&b;
    return b*(i - 13) + 13;
}

_Bool is_color(char* cards)
{
    char colors[5] = {cards[1], cards[3], cards[5], cards[7], cards[9]};
    char c = colors[0];
    _Bool b = (1==1);
    for (int i = 1; i < 5; ++i)
        b = b & (colors[i] == c);
    return b;
}


unsigned int score_high_card(int* hash_figures)
{
    unsigned int score = 1296420;
    int j = 4;
    for (int i = 0; i < 13; ++i)
    {
        if (hash_figures[i] == 1)
            score = score + power(13, j--)*i;
    }
    return score;
}

unsigned int score_one_pair(int* hash_figures)
{
    //sans doute problème, à revoir anyway
    unsigned int score = 198180;
    unsigned int i;
    int j = 2;
    for (i = 0; i < 13; ++i)
    {
        int tmp = hash_figures[i];
        score = score + (tmp == 2)*i*power(13, 3);
        score = score + (tmp == 1)*i*power(13, j--);
    }
    return score;
}

unsigned int score_two_pair(int* hash_figures)
{
    unsigned int score = 74628;
    unsigned int i;
    int j = 2;
    for (i = 0; i < 13; ++i)
    {
        int tmp = hash_figures[i];
        score = score + (tmp == 2)*i*power(13, j--);
        score = score + (tmp == 1)*i;
    }
    return score;
    
}

unsigned int score_three(int* hash_figures)
{
    unsigned int score = 19716;
    unsigned int i;
    int j = 2;
    for (i = 0; i < 13;++i)
    {
        int tmp = hash_figures[i];
        score = score + (tmp == 3)*i*169;
        score = score + (tmp == 1)*power(13, --j);
    }
    return score;  
}

unsigned int score_straight_flush(int s)
{
    return s;
}

unsigned int score_straight(char* cards, int s)
{
    if (is_color(cards))
        return score_straight_flush(s);
    return 9516 + s;
}

unsigned int score_flush(int* hash_figures)
{
    unsigned int score = 4407;
    unsigned int i, j;
    unsigned int better_choice = 12;
    j = 0;
    for (i = 0; i < 13; ++i)
    {
        if (hash_figures[i] == 1)
        {
            score = score + (unsigned int)choose(better_choice, 4 - j++)*i;
            better_choice = (better_choice == 12)*13 + i - better_choice - 1;
        }
    }
    return score;
}

unsigned int score_full(int* hash_figures)
{
    unsigned int score = 664;
    for (int i = 0; i < 13; ++i)
        score = score + (hash_figures[i] == 2)*i + (hash_figures[i] == 3)*13*i;
    return score;
}

unsigned int score_four(int* hash_figures)
{
    unsigned int score = 39;
    for (int i = 0; i < 13; ++i)
        score = score + (hash_figures[i] == 4)*14*i + (hash_figures[i] == 1)*i;
    return score;
}

unsigned int score(char* cards)
{
/*     modifier cette fonction pour 
    qu'elle renvoie max_int en cas de main invalide
    comme ayant cinq as ou deux rois de la même couleur */
    char figures[5] = {cards[0], cards[2], cards[4], cards[6], cards[8]};

    int hash_figures[13] = {0};
    for (int i = 0; i < 5; ++ i)
        hash_figures[value(figures[i])]++;
    int max, distinct;
    max = 0;
    distinct = 0;
    for (int i = 0; i < 13; ++i)
    {
        max = (hash_figures[i] > max)*(hash_figures[i] - max) + max;
        distinct += (hash_figures[i] > 0);
    }

    if (distinct == 4)
        return score_one_pair(hash_figures);
    if (max == 2)
        return score_two_pair(hash_figures);
    if (max == 3 & distinct == 3)
        return score_three(hash_figures);
    int s = is_straight(hash_figures);
    if (s < 13)
        return score_straight(cards, s);
    if (is_color(cards))
        return score_flush(hash_figures);
    if (max == 3 & distinct == 2)
        return score_full(hash_figures);
    if (max == 4)
        return score_four(hash_figures);
    return score_high_card(hash_figures);
}

_Bool test_score(void)
{
    unsigned int b = score("2S3H5STD9C");
    unsigned int tmp = score("AS3HKSTD9C");
    b = (b > tmp)*tmp;

    tmp = score("2S2H5STD9C"); //shitty double
    b = (b > tmp)*tmp;

    tmp = score("2S5H5S3D9C"); //better double
    b = (b > tmp)*tmp;

    tmp = score("2S2H5STDTC"); //double double
    b = (b > tmp)*tmp;

    tmp = score("2S2H5SQD2C"); //three
    b = (b > tmp)*tmp;
    tmp = score("2S5H5STD5C"); //better three
    b = (b > tmp)*tmp;

    tmp = score("2S3H5S4D6C"); //straight
    b = (b > tmp)*tmp;
    tmp = score("7S9H8STD6C"); //better straight

    b = (b > tmp)*tmp;
    tmp = score("2C3C5CTC9C"); //flush
    b = (b > tmp)*tmp;
    tmp = score("2S2HQSQD2C"); //full
    b = (b > tmp)*tmp;
    tmp = score("TSTH3S3DTC"); //better full
    b = (b > tmp)*tmp;
    return (_Bool)b;
}

