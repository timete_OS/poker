/*==================
Timothée Obrecht
sometime around august 2021
==================*/




/*
this file makes extensive use of the score function describe in score.c
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <make.h>



char royal_flush[10] = "ASKSQSJSTS";

char random_hand[10] = "2S2H5H6S7S";

char almost_full[10] = "4H4S4CKS2H";

char min_flush[10] = "ASKSQSJS9S";

char deck[52*2] = "ASAHADACKSKHKDKCQSQHQDQCJSJHJDJCTSTHTDTC9S9H9D9C8S8H8D8C7S7H7D7C6S6H6D6C5S5H5D5C4S4H4D4C3S3H3D3C2S2H2D2C";

void print_hand(char* hand)
{
    for (int i = 0; i < 8; i+=2)
        printf("%c%c ", hand[i], hand[i+1]);
    printf("%c%c\n", hand[8], hand[9]);
    return;
}

void print_cards(char* cards)
{
    int i = 0;
    if (cards[i] == 0)
        return;
    int j = 0;
    printf("%c", cards[0]);
    while (cards[++i] != 0)
    {
        if (++j%2 == 0)
            printf(" ");
        printf("%c", cards[i]);
    }
    printf("\n");
    return;
}


void all_card_combination(unsigned long int* exp, char* stable_cards, char* real_deck, char* pick, int n, int k, int index, int i)
{
/*  just add every score to exp
    yes it uses recursion. I kwnow. sry.  */
    if (index >= k)
    {
        char tmp[11];
        strcpy(tmp, stable_cards);
        strcat(tmp, pick);
        *exp += score(tmp);
        return;
    }
    if (i >= n)
        return;
    
    pick[index] = real_deck[i];
    pick[index+1] = real_deck[i+1];
    all_card_combination(exp, stable_cards, real_deck, pick, n, k, index + 2, i + 2);
    all_card_combination(exp, stable_cards, real_deck, pick, n, k, index, i + 2);

    return;
}

float expectency(char* hand, char* stable_cards)
/*  go through all combination of possible hands after the change, and 
    return the expected average score of those changes as a float */
{
    char* real_deck = malloc(sizeof(char)*47*2 + 1);
    //the main deck needs to be purge from already in hand cards
    int l = strlen(deck);
    for (int i = 0; i<l; i+=2)
    {
        char tmp_str[3] = {deck[i], deck[i+1], '\0'};
        if (strstr(hand, tmp_str) == NULL)
            strcat(real_deck, tmp_str);
    }
    real_deck[47*2] = 0;
    l = strlen(stable_cards);
    char* pick = malloc(sizeof(char)*(10 - l) + 1);
    pick[10 - l] = 0;
    unsigned long int exp = 0;
    all_card_combination(&exp, stable_cards, real_deck, pick, strlen(real_deck), 10-l, 0, 0);
    //max exp now contain the sum of all possible scores
    int m = strlen(real_deck)/2;
    free(real_deck);
    //the number of possible hands is binomial(cards in deck, number of changed cards)
    //we divide exp by this number
    return exp/(float)choose(m, (10-l)/2);
}

void all_change_combination(float* max_score, char* changed_cards, char* hand, char* stable_cards, int n, int k, int index, int i)
{
/*     recursive function going through all possible combination of changed cards
    and keeping in changed_cars and max_score only the best possible combination
    and it's associate score*/
    if (index == k)
    {
        float ex = expectency
    (hand, stable_cards);
        if (ex < *max_score)
        {
            (*max_score) = ex;
            strcpy(changed_cards, stable_cards);
        }
        return;
    }
    if (i >= n)
        return;
    stable_cards[index] = hand[i];
    stable_cards[index+1] = hand[i+1];
    all_change_combination(max_score, changed_cards, hand, stable_cards, n, k, index + 2, i + 2);
    all_change_combination(max_score, changed_cards, hand, stable_cards, n, k, index, i + 2);
    return;
}

char* best_change(char* hand)
{
/*     takes as an input a string describing a valid hand.
    there aren't any checks for valid input

    return in a string cards that needs to be changed to maximize
    score expectency
    
    hand :  */
    float max_score = (float)score(hand);

    char* changed_cards = malloc(9);
    changed_cards[0] = 0;
    changed_cards[8] = 0;
    for (int i = 4; i > 1; --i)
    {
        char* stable_cards = malloc(sizeof(char)*i*2 + 1);
        stable_cards[i*2] = 0;
        all_change_combination(&max_score, changed_cards, hand, stable_cards, strlen(hand), i*2, 0, 0);
        free(stable_cards);
    }
    return changed_cards;
}


int main(int argc, char** argv)
{
/*     a valid input is expected as a first argument
    the answer is printed on the standard output */

    if (argc < 2)
        return 0;
    char* hand = argv[1];
    print_cards(best_change(hand));

    return 0;
}